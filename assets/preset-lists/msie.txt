# User-maintained list: Microsoft Internet Explorer
# =================================================
# Source: https://gitlab.com/ntninja/user-agent-switcher/issues/61
#
# This list may be edited by visiting (GitLab login required)
# https://gitlab.com/ntninja/user-agent-switcher/edit/master/assets/preset-lists/msie.txt
# and submitting a merge request.
#
IE 10 / Win 10 x64 [IE Win]: Mozilla/5.0 (MSIE 10.0; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240
IE 11 / Win 8.1 [IE Win]: Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko
IE 8 / Win 8 [IE Win]: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.2; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)
IE 11 / Win 7 x64 [IE Win]: Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko
IE 10 / Win 7 [IE Win]: Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0
IE 9 / Win Vista [IE Win]: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0; chromeframe/11.0.696.57)
IE 8 / Win XP x64 [IE Win]: Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; Media Center PC 4.0; SLCC1; .NET CLR 3.0.04320)
IE 7 / Win XP x64 [IE Win]: Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 5.2; WOW64; .NET CLR 2.0.50727)
IE 8 / Win XP [IE Win]: Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; SLCC1; .NET CLR 1.1.4322)
IE 6 / Win XP [IE Win]: Mozilla/4.0 (MSIE 6.0; Windows NT 5.1)